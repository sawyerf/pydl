import socket
import ssl
import time
import re

class	Download():
	def __init__(self, host, url='/', headers={}, lang='en', user_agent='Mozilla/5.0'):
		self.url = url
		self.host = host
		self.lang = lang
		self.headers = headers
		self.user_agent = user_agent
		self.headr = ''
		self.response = b''
		self.status = 0

	def __str__(self):
		return self.response.decode()

	def request(self):
		rq = "GET {} HTTP/1.1\r\n".format(self.url)
		rq += "Host: {}\r\n".format(self.host)
		rq += "User-agent: {}\r\n".format(self.user_agent)
		rq += "Accept-Language: {}\r\n".format(self.lang)
		rq += "Accept: */*\r\n"
		for hd in self.headers:
			rq += hd + ': ' + self.headers[hd] + '\r\n'
		rq += '\r\n'
		return rq.encode()

	def get_status(self):
		try:
			self.status = int(re.findall(r'HTTP/1.1 (.\d{1,}) ', self.headr)[0])
		except:
			self.status = -1

	def recv_resp(self, sock, lenght):
		lenres = len(self.response)
		while True:
			try:
				raw_data = sock.recv(8192)
			except ConnectionResetError:
				print('[!] Connection losted ({}{})'.format(self.host, self.url))
				return False
			except:
				return False
			lenres += len(raw_data)
			self.response += raw_data
			if (lenght >= 0 and lenres >= lenght) or raw_data == b'' or b'\r\n0\r\n\r\n' in self.response[-20:]:
				return True

	def recv_head(self, sock):
		data = b''
		endh = False
		while True:
			try:
				raw_data = sock.recv(1000)
			except ConnectionResetError:
				print('[!] Connection losted ({}{})'.format(self.host, self.url))
				return False
			except:
				return False
			data += raw_data
			if b'\r\n\r\n' in data:
				break
		for i in data.split(b'\n'):
			self.headr += i.decode() + '\n'
			if i == b'\r':
				break;
		self.response = data[len(self.headr):]
		self.get_status()
		return True

	def recv(self, sock):
		if not self.recv_head(sock):
			return
		if self.status != 200:
			return
		try:
			lenght = int(re.findall(r'Content-Length: (.*\d)', self.headr)[0])
		except:
			lenght = -1
		if not self.recv_resp(sock, lenght):
			return
		data = b''
		if 'Transfer-Encoding: chunked' in self.headr:
			for i in self.response.split(b'\r\n'):
				if len(i) > 5:
					data += i
			self.response = data
						

	def get_http(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((self.host, 80))
		sock.send(self.request())
		self.recv(sock)
		sock.close()

	def get_https(self):
		data = b''
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((self.host, 443))
		for i in range(5):
			try:
				ssock = ssl.wrap_socket(sock)
			except OSError:
				time.sleep(1)
				if i == 4:
					print('[!] Can\'t start the ssl connection ({}{})'.format(self.host, self.url))
					return ''
			else:
				break
		ssock.write(self.request())
		data = self.recv(ssock)
		ssock.close()

lol = Download('duckduckgo.com')
# print('\33[31m' + lol.request().decode() + '\33[0;0m')
lol.get_https()
print('\33[34m' + lol.headr + '\33[0;0m')
print('\33[1;37m' + str(len(lol.response)) + '\33[0;0m')
# print('\33[1;31m', lol.status, '\33[1;0m')
